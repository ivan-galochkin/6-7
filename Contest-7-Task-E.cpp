#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

struct GraphData {
  vector<vector<int>> adj, nomr;
  vector<int> visited;
  vector<int> mosts;
  vector<int> timein;
  vector<int> up;
  vector<int> pred;
  int t = 0;
};

void Dfs(int v, GraphData& data) {
  ++data.t;
  data.visited[v] = 1;
  data.timein[v] = data.t;
  data.up[v] = data.t;
  int s = data.adj[v].size();
  for (int i = 0; i < s; ++i) {
    if (data.adj[v][i] == data.pred[v]) {
      continue;
    }
    if (data.visited[data.adj[v][i]] == 1) {
      data.up[v] = min(data.up[v], data.timein[data.adj[v][i]]);
    }
    if (data.visited[data.adj[v][i]] == 0) {
      data.pred[data.adj[v][i]] = v;
      Dfs(data.adj[v][i], data);
      data.up[v] = min(data.up[v], data.up[data.adj[v][i]]);
      if (data.up[data.adj[v][i]] > data.timein[v]) {
        int cnt = 0;
        for (int j = 0; j < s; ++j) {
          if (data.adj[v][j] == data.adj[v][i]) {
            ++cnt;
          }
        }
        if (cnt == 1) {
          data.mosts.emplace_back(data.nomr[v][i]);
        }
      }
    }
  }
  data.t++;
}

int main() {
  GraphData data;
  int n, m;
  cin >> n >> m;
  for (int i = 0; i < n; ++i) {
    data.adj.emplace_back(vector<int>());
    data.nomr.emplace_back(vector<int>());
  }
  for (int i = 0; i < m; i++) {
    int u, v;
    cin >> u >> v;
    u--;
    v--;
    data.adj[u].push_back(v);
    data.adj[v].emplace_back(u);
    data.nomr[u].emplace_back(i);
    data.nomr[v].emplace_back(i);
  }
  for (int i = 0; i < n; ++i) {
    data.visited.emplace_back(0);
    data.timein.emplace_back(0);
    data.up.emplace_back(0);
    data.pred.emplace_back(0);
  }
  for (int i = 0; i < n; ++i) {
    if (data.visited[i] == 0) {
      Dfs(i, data);
    }
  }
  cout << data.mosts.size() << "\n";
  sort(data.mosts.begin(), data.mosts.end());
  for (int most : data.mosts) {
    cout << most + 1 << ' ';
  }
}
