#include <iostream>
#include <vector>

void PrintResult(int i, int j, std::vector<std::vector<int>> dp,
                 std::vector<int> res) {
  if (dp[i][j] == 0) {
    return;
  }
  if (dp[i][j] == dp[i - 1][j]) {
    PrintResult(i - 1, j, dp, res);
  } else {
    PrintResult(i - 1, j - res[i], dp, res);
    std::cout << i << "\n";
  }
}

int main() {
  int n, m;
  std::cin >> n >> m;
  std::vector<int> res(n + 1, 0);
  std::vector<int> cost(n + 1, 0);
  for (int i = 1; i < n + 1; ++i) {
    std::cin >> res[i];
  }
  for (int i = 1; i < n + 1; ++i) {
    std::cin >> cost[i];
  }
  std::vector<std::vector<int>> dp(n + 1, std::vector<int>(m + 1, 0));

  for (int i = 1; i < n + 1; ++i) {
    for (int j = 1; j < m + 1; ++j) {
      if (j >= res[i]) {
        dp[i][j] = std::max(dp[i - 1][j], dp[i - 1][j - res[i]] + cost[i]);
      } else {
        dp[i][j] = dp[i - 1][j];
      }
    }
  }
  PrintResult(n, m, dp, res);
  return 0;
}
