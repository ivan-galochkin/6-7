#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

struct GraphData {
  vector<vector<int>> adj, adjob;
  vector<int> visited;
  vector<pair<int, int>> timeout;
  vector<int> groups;
  int tout = 0;
  int group = 0;
};

bool Comp(pair<int, int> x, pair<int, int> y) { return x.second > y.second; }

void Dfs(int v, GraphData& g) {
  ++g.tout;
  g.visited[v] = -1;
  for (int u : g.adj[v]) {
    if (g.visited[u] == 0) {
      Dfs(u, g);
    }
  }
  g.timeout[v].second = g.tout++;
  g.visited[v] = 1;
}

void Dfsob(int v, GraphData& g) {
  g.visited[v] = -1;
  for (int u : g.adjob[v]) {
    if (g.visited[u] == 0) {
      Dfsob(u, g);
    }
  }
  g.visited[v] = 1;
  g.groups[v] = g.group;
}

int main() {
  GraphData g;
  int n, m;
  cin >> n >> m;
  for (int i = 0; i < n; ++i) {
    g.adj.emplace_back(vector<int>());
    g.adjob.emplace_back(vector<int>());
  }
  for (int i = 0; i < m; i++) {
    int u, v;
    cin >> u >> v;
    u--;
    v--;
    g.adj[u].push_back(v);
    g.adjob[v].emplace_back(u);
  }
  for (int i = 0; i < n; ++i) {
    g.visited.emplace_back(0);
    g.timeout.emplace_back(make_pair(i, 0));
    g.groups.emplace_back(0);
  }
  for (int v = 0; v < n; v++) {
    if (g.visited[v] == 0) {
      Dfs(v, g);
    }
  }
  sort(g.timeout.begin(), g.timeout.end(), Comp);
  for (int i = 0; i < n; ++i) {
    g.visited[i] = 0;
  }
  for (int i = 0; i < n; ++i) {
    if (g.visited[g.timeout[i].first] == 0) {
      ++g.group;
      Dfsob(g.timeout[i].first, g);
    }
  }
  cout << g.group << "\n";
  for (int i = 0; i < n; ++i) {
    cout << g.groups[i] << ' ';
  }
}