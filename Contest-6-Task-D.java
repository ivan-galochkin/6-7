import java.util.*;

public class Main {
    static ArrayList<ArrayList<Integer>> arr;

    public static void Basic(int n, int k) {
        for (int i = 0; i <= k; ++i) {
            arr.add(new ArrayList<>());
        }
        arr.get(0).add(1);
        for (int j = 1; j < 2 * n; j++) {
            arr.get(0).add(0);
        }
        if (k > 0) {
            for (int j = 0; j < 2 * n; j++) {
                arr.get(1).add(j + 1);
            }
        }
    }
	
    
    public static int MinExpCount(int n, int k) {
        for (int j = 0; j < 2 * n; ++j) {
            for (int i = 0; i < k + 1; ++i) {
                if (i >= 2) {
                    if (j == 0) {
                        arr.get(i).add(1);
                    } else {
                        arr.get(i).add(arr.get(i).get(j - 1) + arr.get(i - 1).get(j - 1));
                    }

                }
                if (i == k && arr.get(k).get(j) >= n) {
                    return j;
                }
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int k = sc.nextInt();
        
        arr = new ArrayList<>(k + 1);
        
        Basic(n, k);
        System.out.println(MinExpCount(n, k));
    }
}