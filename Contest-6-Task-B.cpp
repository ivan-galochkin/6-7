#include <algorithm>
#include <climits>
#include <iostream>
#include <vector>

bool Comp(const int& a, const int& b) { return a >= b; }

class Decision {
 private:
  static void Solve(std::vector<int> numbers) {
    int n = numbers.size();
    int maxi = 0;
    std::vector<int> dp(n + 1, INT_MIN);
    dp[0] = INT_MAX;

    std::vector<int> index(n + 1, 0);
    std::vector<int> pr(n + 1, 0);

    index[0] = -1;

    for (int i = 0; i < n; ++i) {
      int pos =
          upper_bound(dp.begin(), dp.end(), numbers[i], Comp) - dp.begin();
      if (dp[pos - 1] >= numbers[i] && numbers[i] >= dp[pos]) {
        dp[pos] = numbers[i];
        index[pos] = i;
        pr[i] = index[pos - 1];
        maxi = std::max(maxi, pos);
      }
    }
    std::vector<int> ans;
    int p = index[maxi];
    while (p != -1) {
      ans.emplace_back(p);
      p = pr[p];
    }
    std::cout << maxi << "\n";
    for (int i = maxi - 1; i >= 0; --i) {
      std::cout << ans[i] + 1 << " ";
    }
  }

 public:
  static void Run() {
    int n, a;
    std::vector<int> numbers;

    std::cin >> n;
    for (int i = 0; i < n; ++i) {
      std::cin >> a;
      numbers.emplace_back(a);
    }
    Solve(numbers);
  }
};

int main() {
  Decision::Run();
  return 0;
}
